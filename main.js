const puppeteer = require('puppeteer');
const exec = require('child_process').exec;
const fs = require('fs');
const os = require('os');
const commandlineMode = (process.env.CMD || 'false') === 'true';
console.log('commandline mode enabled:', commandlineMode);

(async () => {
  let activeProxyIndex = 0;
  let browser;
  const proxiesList = addProxiesFromFile();
  const userAgentList = getUserAgents()

  const buttonSelector = 'body > app-root > div > app-page-its-login > div > div > div:nth-child(2) > app-its-login-user > div > div > app-corona-vaccination > div:nth-child(2) > div > div > label:nth-child(2) > span'

  function addProxiesFromFile() {
    const proxyPath = __dirname + '/proxies.json'
    const proxyFile = fs.existsSync(proxyPath)
    const ownMachine = {ip: '', active: true}
    if (!proxyFile)
      return [ownMachine]
    const {proxies} = JSON.parse(fs.readFileSync(proxyPath).toString());
    console.log(`added additional ${proxies.length} proxies`)
    return [ownMachine, ...proxies]
  }

  function getRandomItem(array) {
    if (array.length === 0) return array[0]
    const random = Math.floor(Math.random() * array.length);
    return array[random]
  }

  function getUserAgents() {
    const userAgentPath = __dirname + '/user_agents.json'
    const userAgentFile = fs.existsSync(userAgentPath)
    if (!userAgentFile)
      return ['Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36'];
    const {userAgents} = JSON.parse(fs.readFileSync(userAgentPath).toString())
    return userAgents
  }

  async function getNewBrowser(proxy) {
    const userAgent = getRandomItem(userAgentList)
    console.log(`using user agent: ${userAgent}`)
    const args = [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-infobars',
      '--ignore-certifcate-errors',
      '--ignore-certifcate-errors-spki-list',
      `--user-agent="${userAgent}"`
    ]
    if (proxy !== '') {
      args.push(`--proxy-server=${proxy}`)
    }
    browser = await puppeteer.launch({
      slowMo: 100,
      headless: commandlineMode,
      // devtools: true,
      args
    })
    const [page] = await browser.pages()
    await page.setViewport({
      width: 1920,
      height: 1080,
      deviceScaleFactor: 1
    })
    await page.setRequestInterception(true)
    page.on('request', (interceptedRequest) => {
      if (interceptedRequest.url().includes('/suche/termincheck?plz=20357&leistungsmerkmale'))
        console.log(`current proxy '${proxy}':`, interceptedRequest.url())
      interceptedRequest.continue()
    })
    await page.goto('https://353-iz.impfterminservice.de/impftermine/service?plz=20357')
    await page.waitForSelector(buttonSelector)
    return page
  }

  async function changeProxy(page) {
    const activeProxies = proxiesList.filter(proxy => proxy.active)
    activeProxyIndex++
    console.log('changing proxy:', activeProxies[activeProxyIndex])
    if (activeProxyIndex > activeProxies.length-1)
      activeProxyIndex = 0
    await page.browser().close()
    return getNewBrowser(activeProxies[activeProxyIndex].ip)
  }

  async function poll() {
    let run = true
    const activeProxies = proxiesList.filter(proxy => proxy.active)
    let page = await getNewBrowser(activeProxies[activeProxyIndex].ip)
    while (run) {
      await page.click(buttonSelector)
      const finalRequest = await page.waitForResponse(
          (request) => request.url().includes('/suche/termincheck?plz=20357&leistungsmerkmale')
      )
      const res = await finalRequest.json()
      console.log(res)
      if (Object.keys(res).length === 0) {
        page = await changeProxy(page)
        continue
      }
      if (res.termineVorhanden) {
        console.log('🎉 appointment found!! 🎉')
        console.log('https://353-iz.impfterminservice.de/impftermine/service?plz=20357')
        run = false
        if (!commandlineMode) {
          exec("notify-send \"🎉 appointments found!! 🎉\"")
          exec('google-chome "https://353-iz.impfterminservice.de/impftermine/suche/YLT7-7PZR-JDF6/20357/"')
          exec("paplay /usr/share/sounds/gnome/default/alerts/bark.ogg")
          await page.waitForTimeout(1000)
          exec("paplay /usr/share/sounds/gnome/default/alerts/bark.ogg")
          await page.waitForTimeout(1000)
          exec("paplay /usr/share/sounds/gnome/default/alerts/bark.ogg")
        }
      }
      await page.waitForTimeout(5000)
    }
  }

  while (true) {
    await poll().catch(async (err) => {
      console.log(err.stack)
      const activeProxies = proxiesList.filter(proxy => proxy.active)
      if (activeProxies.length === 1) {
        console.log('all proxy are inactive. aborting...')
        os.exit(1)
      }
      const proxyWithError = activeProxies[activeProxyIndex]
      console.log(`turn off proxy: ${proxyWithError.ip}`)
      proxyWithError.active = false
      console.log('activeProxies:', activeProxies.length)
      activeProxyIndex++
      console.log('changing proxy:', activeProxies[activeProxyIndex])
      await browser.close()
    })
  }
})()
